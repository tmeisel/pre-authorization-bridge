package main

import (
	"context"
	log "github.com/sirupsen/logrus"
	ctxPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/ctx"
	logPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/logr"
	"preauthbridge/internal/api"
	"preauthbridge/internal/config"
	"preauthbridge/internal/database"
	"preauthbridge/internal/security"
)

func main() {
	ctx := context.Background()

	err := config.LoadConfig()
	if err != nil {
		log.Fatalf("failed to load config: %v", err)
	}

	conf := config.CurrentPreAuthBridgeConfig

	logger, err := logPkg.New(conf.LogLevel, conf.IsDev, nil)
	if err != nil {
		log.Fatalf("failed to init logger: %v", err)
	}

	ctx = ctxPkg.WithLogger(ctx, *logger)

	dbConfig := &config.CurrentPreAuthBridgeConfig.Database
	dbConnection, err := database.New(*dbConfig, ctx)
	if err != nil {
		log.Fatal(err)
	}

	auth := security.New(dbConnection)

	api.Listen(auth)
}
