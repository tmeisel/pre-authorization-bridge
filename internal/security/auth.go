package security

import (
	"context"
	"errors"
	"fmt"
	"preauthbridge/internal/database"
	"preauthbridge/internal/entity"
	"preauthbridge/pkg/generator"
	"time"
)

const codeLength = 20
const pinLength = 6

type Auth struct {
	dbConnection database.DbConnection
}

func New(dbConnection database.DbConnection) *Auth {
	return &Auth{dbConnection: dbConnection}
}

func (a *Auth) Generate(ctx context.Context, withPin bool, ttl time.Duration) (*entity.Authentication, error) {
	code, err := newCode()
	if err != nil {
		return nil, fmt.Errorf("error occured while generating new authCode: %w", err)
	}

	var pin string
	if withPin {
		pin, err = newPin()
		if err != nil {
			return nil, fmt.Errorf("error occured while generating new authPin: %w", err)
		}
	} else {
		pin = ""
	}

	newAuth := &entity.Authentication{
		Code: code,
		Pin:  pin,
	}

	if err := a.dbConnection.SaveAuth(ctx, newAuth, ttl); err != nil {
		return nil, fmt.Errorf("error occured while saving auth to database: %w", err)
	}

	return newAuth, nil
}

func (a *Auth) Check(ctx context.Context, auth *entity.Authentication) (bool, error) {
	validPin, err := a.dbConnection.GetPinOfAuthCode(ctx, auth.Code)
	if errors.Is(err, database.ErrKeyNotFound) {
		// key does not exist in the database anymore -> ttl is over
		return false, nil
	} else if err != nil {
		return false, fmt.Errorf("error occured while retrieving pin from database for authCode %s: %w", auth.Code, err)
	}

	//TODO: check: when incoming auth has pin and in database pin not set -> not valid?
	switch validPin {
	case "":
		// no pin set -> authCode without pin
		return true, nil
	case auth.Pin:
		// provided authPin and pin in database are equal
		return true, nil
	default:
		// provided authPin and pin in database are not equal
		return false, nil
	}
}

func (a *Auth) Delete(ctx context.Context, auth *entity.Authentication) (bool, error) {
	_, err := a.dbConnection.DeletePinOfAuthCode(ctx, auth.Code)

	if errors.Is(err, database.ErrKeyNotFound) {
		// key does not exist in the database anymore -> ttl is over
		return false, nil
	} else if err != nil {
		return false, fmt.Errorf("error occured while deleting pin from database for authCode %s: %w", auth.Code, err)
	}

	return true, nil
}

func newCode() (string, error) {
	code, err := generator.RandomCode(codeLength, generator.Characters, generator.Numbers)
	if err != nil {
		return "", err
	}
	return code, nil
}

func newPin() (string, error) {
	code, err := generator.RandomCode(pinLength, generator.Numbers)
	if err != nil {
		return "", err
	}
	return code, nil
}
