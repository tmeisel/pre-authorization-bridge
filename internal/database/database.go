package database

import (
	"context"
	"fmt"
	redisPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/db/redis"
	"preauthbridge/internal/entity"
	"time"
)

var ErrKeyNotFound = fmt.Errorf("key not in database")

type DbConnection interface {
	SaveAuth(ctx context.Context, authentication *entity.Authentication, ttl time.Duration) error
	GetPinOfAuthCode(ctx context.Context, authCode string) (string, error)
	DeletePinOfAuthCode(ctx context.Context, authCode string) (bool, error)
}

type Database struct {
	DbConnection
}

func New(config redisPkg.Config, ctx context.Context) (*Database, error) {
	dbConnection, err := newRedisConnection(config, ctx)
	if err != nil {
		return nil, fmt.Errorf("could not create redis connection: %w", err)
	}
	return &Database{DbConnection: dbConnection}, nil
}
