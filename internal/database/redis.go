package database

import (
	"context"
	"github.com/redis/go-redis/v9"
	ctxPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/ctx"
	redisPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/db/redis"
	errPkg "gitlab.eclipse.org/eclipse/xfsc/libraries/microservice/core/pkg/err"
	"os"

	"preauthbridge/internal/entity"

	"time"
)

type redisConnection struct {
	client *redis.Client
}

func newRedisConnection(database redisPkg.Config, ctx context.Context) (*redisConnection, error) {
	logger := ctxPkg.GetLogger(ctx)

	errChan := make(chan error)
	go errPkg.LogChan(logger, errChan)

	conn, err := redisPkg.ConnectRetry(ctx, database, time.Minute, errChan)

	if err != nil {
		logger.Error(err, "failed to connect to postgres")
		os.Exit(1)
	}

	return &redisConnection{
		client: conn,
	}, nil
}

func (r *redisConnection) SaveAuth(ctx context.Context, authentication *entity.Authentication, ttl time.Duration) error {
	if err := r.client.Set(ctx, authentication.Code, authentication.Pin, ttl).Err(); err != nil {
		return err
	}
	return nil
}

func (r *redisConnection) GetPinOfAuthCode(ctx context.Context, authCode string) (string, error) {
	pin, err := r.client.Get(ctx, authCode).Result()
	if err == redis.Nil {
		return "", ErrKeyNotFound
	} else if err != nil {
		return "", err
	}
	return pin, nil
}

func (r *redisConnection) DeletePinOfAuthCode(ctx context.Context, authCode string) (bool, error) {
	result, err := r.client.Del(ctx, authCode).Result()
	if err == redis.Nil {
		return false, ErrKeyNotFound
	} else if err != nil {
		return false, err
	}

	if result > 0 {
		return true, nil
	}

	return false, nil
}
