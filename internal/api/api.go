package api

import (
	"preauthbridge/internal/config"
	"preauthbridge/internal/security"
	"sync"
)

var auth *security.Auth

func Listen(a *security.Auth) {
	var wg sync.WaitGroup

	auth = a

	wg.Add(2)
	go startMessaging(&wg)
	go startRest(config.CurrentPreAuthBridgeConfig.Port, &wg)

	wg.Wait()
}
