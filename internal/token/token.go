package token

import (
	"context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
	"preauthbridge/internal/config"
)

func New(ctx context.Context) (*oauth2.Token, error) {
	oAuthConfig := config.CurrentPreAuthBridgeConfig.OAuth

	tokenConfig := clientcredentials.Config{
		ClientID:       oAuthConfig.ClientId,
		ClientSecret:   oAuthConfig.ClientSecret,
		TokenURL:       oAuthConfig.ServerUrl,
		Scopes:         nil,
		EndpointParams: nil,
		AuthStyle:      0,
	}

	token, err := tokenConfig.Token(ctx)
	if err != nil {
		return nil, err
	}

	return token, nil
}
